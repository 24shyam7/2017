package smr.hw.domain;

import java.text.SimpleDateFormat;
import java.util.logging.SimpleFormatter;

public class Employee {
	private String id;
	private String firstName;
	private String lastName;
	private String managerId;
	private String departmentId;
	private String email;
	private String phoneNumber;
	private java.util.Date hire_date;
	private double salary;
	private double commission;
	
	public String formattedDate()
	{
		SimpleDateFormat fmt = new SimpleDateFormat("MM-dd-yyyy");
		return fmt.format(hire_date);
		
	}
	
	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public java.util.Date getHire_date() {
		return hire_date;
	}

	public void setHire_date(java.util.Date hire_date) {
		this.hire_date = hire_date;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", managerId=" + managerId
				+ ", departmentId=" + departmentId + ", email=" + email + ", phoneNumber=" + phoneNumber
				+ ", hire_date=" + hire_date + ", salary=" + salary + ", commission=" + commission + "]";
	}
	
	
	
	
}
