package smr.hw.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.PdfWriter;

import ch.qos.logback.classic.Logger;

public class Scenario4 {
	
	private Connection con;
	private PreparedStatement ps = null;
	private ResultSet rs = null;
	Properties mainprops;
	
	
	
	public Scenario4(Connection con, Properties mainprops) {
		super();
		this.con = con;
		this.mainprops = mainprops;
	}


	private static final Logger log = (Logger) LoggerFactory.getLogger(Scenario4.class);
	

	//generate email with attachment pdf scenario 4
		/*
		scenario 4
		----------

		calculate payroll ordered by department for employee id with commission calculated and added to salary
		*/
		public void writeToVelocitySc4Template() throws SQLException, DocumentException, IOException, AddressException, MessagingException {
			// query db retrieve resultset
			if(con != null)
			{
				ps = con.prepareStatement(mainprops.getProperty("query5"));
				rs = ps.executeQuery();
				Map<String,String> sc4Map = new HashMap<String,String>();
				List<Map<String,String>> sc4List = new ArrayList<>();
				List<String> titles = new ArrayList<String>();
				titles.add("TOTAL MONTHLY CTC ($)");
				titles.add("DEPARTMENT NAME");
				while(rs.next())
				{
					sc4Map.put("total", rs.getString(1));
					sc4Map.put("deptName",rs.getString(2));
					sc4List.add(sc4Map);
				}
				
				
			// prepare a velocity template that generates pdf
				VelocityEngine ve = new VelocityEngine();
				ve.init(mainprops);
				Template t = ve.getTemplate("templates/deptCost.vm");
				
				VelocityContext context = new VelocityContext();
				context.put("T1_CONTENT", sc4List);
				context.put("T1_TITLES",titles);
				
				StringWriter writer = new StringWriter();
				
				t.merge(context, writer);
			
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(new File(mainprops.getProperty("location")
						+mainprops.getProperty("filename5")+mainprops.getProperty("extension5"))));
				document.open();
				document.addAuthor("shyam");
				
				HTMLWorker worker = new HTMLWorker(document);
				worker.parse(new StringReader(writer.toString()));
				document.close();
				
				
				Path uploadFile =  Paths.get(mainprops.getProperty("location")
						+mainprops.getProperty("filename5")+mainprops.getProperty("extension5"));
				
			// prepare email
				final String username = mainprops.getProperty("mail.from");
				final String password = mainprops.getProperty("mail.pass");
				
				
				Session session = Session.getInstance(mainprops,
				          new javax.mail.Authenticator() {
				            protected PasswordAuthentication getPasswordAuthentication() {
				                return new javax.mail.PasswordAuthentication(username, password);
				            }
				          });
				Message message = new MimeMessage(session);
				
				message.setFrom(new InternetAddress(username));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("shyam_@live.in"));
				message.setSubject(mainprops.getProperty("mail.subject"));
				
				Multipart multipart = new MimeMultipart();
				
				MimeBodyPart bodyPart = new MimeBodyPart();
				bodyPart.attachFile(uploadFile.toFile());
				bodyPart.setFileName(uploadFile.toFile().getName());
				String messageBody = "<html><body><h1>email with pdf attachment</h1></body></html>";
				log.info(messageBody);
				multipart.addBodyPart(bodyPart);
				MimeBodyPart bodyPart2 = new MimeBodyPart();
				bodyPart2.setContent(messageBody,"text/html; charset=utf-8");
				multipart.addBodyPart(bodyPart2);
				
				
				
				message.setContent(multipart);
			// send email
				Transport.send(message);
				log.info("message sent");
				log.info(new Date().toString());
			}
			else
			{
				log.error("connection issues");
			}
		}
}
