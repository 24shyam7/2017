package smr.hw.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import smr.hw.domain.Employee;

//generate excel sheets with apache velocity with scenario 1
	/*
	scenario1
	---------
	Print employee details -  one on each page.

	first name
	last name
	employee id
	email
	phone number
	hire_date 
	salary
	commission pct
	manager id
	department id
	*/
public class Scenario1 {

	private Connection con;
	private PreparedStatement ps = null;
	private ResultSet rs = null;
	Properties mainprops;
	
	private static final Logger log = (Logger) LoggerFactory.getLogger(Scenario1.class);
	
	
	
	public Scenario1(Connection con, Properties mainprops) {
		super();
		this.con = con;
		this.mainprops = mainprops;
	}

	
	public void writeToSc1VelocityTemplate() throws SQLException, IOException, InvalidFormatException {
		// make the query obtain the result
		String location = mainprops.getProperty("location");
		String filename = mainprops.getProperty("filename2")+Runner.getDateString()+mainprops.getProperty("extension2");
		rs = Runner.executeSQLQuery("query3");
		
		
		XSSFWorkbook wb = new XSSFWorkbook();
		
		
		List<Employee> empList = new ArrayList<Employee>();
		List<String> titles = new ArrayList<String>();
		if(rs != null)
		{
			while(rs.next())
			{
				Employee e = new Employee();
				e.setId(rs.getString("EMPLOYEE_ID"));
				e.setFirstName(rs.getString("FIRST_NAME"));
				e.setLastName(rs.getString("LAST_NAME"));
				e.setEmail(rs.getString("EMAIL"));
				e.setPhoneNumber(rs.getString("PHONE_NUMBER"));
				e.setHire_date(new Date(rs.getDate("HIRE_DATE").getTime()));
				e.setSalary(rs.getDouble("SALARY"));
				e.setCommission(rs.getDouble("COMMISSION_PCT"));
				e.setManagerId(rs.getString("MANAGER_ID"));
				e.setDepartmentId(rs.getString("DEPARTMENT_ID"));
				log.info(e.toString());
				empList.add(e);
			}
			log.info("emp list fully loaded");
			titles.add("EMPLOYEE ID");
			titles.add("FIRST NAME");
			titles.add("LAST NAME");
			titles.add("EMAIL");
			titles.add("PHONE NUMBER");
			titles.add("HIRE DATE");
			titles.add("SALARY");
			titles.add("COMMISSION %");
			titles.add("MANAGER ID");
			titles.add("DEPARTMENT ID");
			for(int i=0;i<empList.size();i++)
			{
				XSSFSheet sheet = wb.createSheet();
				short index = 0;
				Row row = sheet.createRow(index);
				Runner.createCell(wb, row, (short) 0,titles.get(index++),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				Runner.createCell(wb, row, (short) 1,empList.get(i).getId(),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				
				row = sheet.createRow(index);
				Runner.createCell(wb, row, (short) 0,titles.get(index++),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				Runner.createCell(wb, row, (short) 1,empList.get(i).getFirstName(),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				
				row = sheet.createRow(index);
				Runner.createCell(wb, row, (short) 0,titles.get(index++),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				Runner.createCell(wb, row, (short) 1,empList.get(i).getLastName(),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				row = sheet.createRow(index);
				Runner.createCell(wb, row, (short) 0,titles.get(index++),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				Runner.createCell(wb, row, (short) 1,empList.get(i).getEmail(),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				
				row = sheet.createRow(index);
				Runner.createCell(wb, row, (short) 0,titles.get(index++),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				Runner.createCell(wb, row, (short) 1,empList.get(i).getPhoneNumber(),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				
				row = sheet.createRow(index);
				Runner.createCell(wb, row, (short) 0,titles.get(index++),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				Runner.createCell(wb, row, (short) 1,empList.get(i).formattedDate(),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				
				row = sheet.createRow(index);
				Runner.createCell(wb, row, (short) 0,titles.get(index++),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				Runner.createCell(wb, row, (short) 1,String.valueOf(empList.get(i).getSalary()),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				
				row = sheet.createRow(index);
				Runner.createCell(wb, row, (short) 0,titles.get(index++),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				Runner.createCell(wb, row, (short) 1,String.valueOf(empList.get(i).getCommission()),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				
				row = sheet.createRow(index);
				Runner.createCell(wb, row, (short) 0,titles.get(index++),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				Runner.createCell(wb, row, (short) 1,empList.get(i).getManagerId(),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				
				row = sheet.createRow(index);
				Runner.createCell(wb, row, (short) 0,titles.get(index++),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				Runner.createCell(wb, row, (short) 1,empList.get(i).getDepartmentId(),CellStyle.ALIGN_CENTER, CellStyle.ALIGN_CENTER);
				
			}
		
		
		
        FileOutputStream fos = new FileOutputStream(new File(location+filename));
        wb.write(fos);
		fos.close();
		log.info("written output to file " + new File(location+filename).getAbsolutePath() );
		}
	}
	

	

}
