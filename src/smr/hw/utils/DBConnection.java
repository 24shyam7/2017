package smr.hw.utils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

public class DBConnection {
	private Properties prop;
	private Connection con;
	private static DBConnection connection;
	
	
	private static final Logger log = (Logger) LoggerFactory.getLogger(DBConnection.class);
	
	
	private DBConnection() {
		
		InputStream is = new ClasspathResourceLoader().getResourceStream("db.properties");
		this.prop = new Properties();
		try {
			this.prop.load(is);
			is.close();
			if(prop.isEmpty())
				log.info("properties file is empty or not loaded correctly");
			else
			{
				String url = prop.getProperty("db.url");
				String username = prop.getProperty("db.username");
				String password = prop.getProperty("db.password");
				//log.info(url+" "+username+" "+password);
				con = DriverManager.getConnection(url,username,password);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public static Connection getConnection()
	{
		if(connection == null)
			connection = new DBConnection();
		
		return connection.con;
	}
	
	
	public static void closeConnection()
	{
		try {
			connection.con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
