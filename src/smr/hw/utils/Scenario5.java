package smr.hw.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.PasswordAuthentication;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

//8. generate email with attachment excel for scenario 5.
/*
 * 
 * scenario 5
 * ----------
 *
 * calculate total payroll cost grouped by department
 *
 */
public class Scenario5 {
	
	
	private Connection con;
	private PreparedStatement ps = null;
	private ResultSet rs = null;
	Properties mainprops;
	
	private static final Logger log = (Logger) LoggerFactory.getLogger(Scenario5.class);
	
	
	
	public Scenario5(Connection con, Properties mainprops) {
		super();
		this.con = con;
		this.mainprops = mainprops;
	}
	
	public void scenario5VelocityTemplate() throws SQLException, IOException, AddressException, MessagingException
	{
		if(con != null)
		{
			ps = con.prepareStatement(mainprops.getProperty("query6"));
			rs = ps.executeQuery();
			
			if(rs != null)
			{
				Map<String,String> queryMap = null;
				List<Map<String,String>> outList = new ArrayList<Map<String,String>>();
				
				
				
				while(rs.next())
				{
					
					queryMap = new HashMap<String,String>();
					queryMap.put("empId", rs.getString(1));
					queryMap.put("salary", rs.getString(2));
					queryMap.put("salary1", rs.getString(3));
					queryMap.put("commission_pct", rs.getString(4));
					
					
					outList.add(queryMap);
				}
				
				// prepare an excel file
				XSSFWorkbook workbook = new XSSFWorkbook();
				Sheet sheet = workbook.createSheet("origin");
				log.info("arraySize : " + outList.size());
				
				short idx = 1;
				Row row = sheet.createRow(idx);
				row.setHeightInPoints(30);
				Runner.createCell(workbook, row, (short)1, "EMPLOYEE ID", CellStyle.ALIGN_CENTER,CellStyle.ALIGN_CENTER);
				Runner.createCell(workbook, row, (short)2, "SALARY", CellStyle.ALIGN_CENTER,CellStyle.ALIGN_CENTER);
				
				
				for(int index = 0 ; index < outList.size() ; index++)
				{
					
					row = sheet.createRow(++idx);
					row.setHeightInPoints(30);
					Runner.createCell(workbook, row, (short)1, outList.get(index).get("empId"), CellStyle.ALIGN_CENTER,CellStyle.ALIGN_CENTER);
					Runner.createCell(workbook, row, (short)2, outList.get(index).get("salary"), CellStyle.ALIGN_CENTER,CellStyle.ALIGN_CENTER);
					
				}
				String location = mainprops.getProperty("location");
				String filename = mainprops.getProperty("filename6");
				String extension = mainprops.getProperty("extension6");
				String username = mainprops.getProperty("mail.from");
				String password = mainprops.getProperty("mail.pass");
		        FileOutputStream fos = new FileOutputStream(new File(location+filename+extension));
		        workbook.write(fos);
				fos.close();
				log.info("workbook created");
				// create an email session
				
				Session session = Session.getInstance(mainprops,new javax.mail.Authenticator(){
					protected javax.mail.PasswordAuthentication getPasswordAuthentication()
					{
						return new javax.mail.PasswordAuthentication(username, password);
					}
				});
				
				
				// prepare and send email
				MimeMessage message = new MimeMessage(session);
				
				message.setFrom(new InternetAddress(username));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("shyam_@live.in"));
				message.setSubject(mainprops.getProperty("mail.subject"));
				
				if("127.0.0.1".equals(InetAddress.getLocalHost().getHostAddress().toString()))
				{
					
					log.info("internet connection ok");
					Multipart multipart = new MimeMultipart();
					
					BodyPart  bodyPart = new MimeBodyPart();
					Path file = Paths.get(location+filename+extension);
					bodyPart.setContent("<html><body><h1>excel attachment</h1></body></html>","text/html; charset=utf-8");
					
					
					MimeBodyPart bodyPart2 = new MimeBodyPart();
					bodyPart2.attachFile(file.toFile());
					multipart.addBodyPart(bodyPart);
					multipart.addBodyPart(bodyPart2);
					message.setContent(multipart);
					
					Transport.send(message);
					log.info("email sent");
				}
				
			}
		}
	}

}
